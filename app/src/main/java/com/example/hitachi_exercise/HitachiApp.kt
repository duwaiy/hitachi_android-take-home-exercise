package com.example.hitachi_exercise

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
open class HitachiApp : Application()