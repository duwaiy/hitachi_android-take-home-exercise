package com.example.hitachi_exercise.data


sealed class Resources<T>(
    val data: T? = null,
    val errorCode: Int? = null,
    val errorDesc: String? = null,
) {

    class Success<T>(data: T) : Resources<T>(data)
    class Loading<T>(data: T? = null) : Resources<T>(data)
    class DataError<T>(errorCode: Int, errorDesc: String) : Resources<T>(null, errorCode, errorDesc)

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is DataError -> "Error[exceptionCode=$errorCode&exceptionDesc=$errorDesc]"
            is Loading<T> -> "Loading"
        }
    }
}