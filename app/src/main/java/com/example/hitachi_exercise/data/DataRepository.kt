package com.example.hitachi_exercise.data

import com.example.hitachi_exercise.data.dto.CityResponse
import com.example.hitachi_exercise.data.remote.RemoteData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class DataRepository @Inject constructor(
    private val remoteRepository: RemoteData,
    private val ioDispatcher: CoroutineContext
) : DataRepositorySource {
    override suspend fun getCity(
        key: String?,
        q: String?,
    ): Flow<Resources<CityResponse>> {
        return flow {


            emit(
                remoteRepository.getCity(
                    key,
                    q
                )
            )
        }.flowOn(ioDispatcher)
    }
}