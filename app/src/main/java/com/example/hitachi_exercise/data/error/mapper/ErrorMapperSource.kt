package com.example.hitachi_exercise.data.error.mapper

interface ErrorMapperSource {
    fun getErrorInt(errorId: Int): String
    fun getErrorString(errorId: String): String
    val errorsMap: Map<Int, String>
}