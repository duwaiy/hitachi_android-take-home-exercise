package com.example.hitachi_exercise.data

import com.example.hitachi_exercise.data.dto.CityResponse
import kotlinx.coroutines.flow.Flow

interface DataRepositorySource {
    suspend fun getCity(
        key: String?,
        q: String?
    ): Flow<Resources<CityResponse>>
}