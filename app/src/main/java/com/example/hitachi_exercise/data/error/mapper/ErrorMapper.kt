package com.example.hitachi_exercise.data.error.mapper

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class ErrorMapper @Inject constructor(
    @ApplicationContext val context: Context,
    override val errorsMap: Map<Int, String>
) : ErrorMapperSource {


    override fun getErrorInt(errorId: Int): String {
        return context.getString(errorId)
    }

    override fun getErrorString(errorId: String): String {
        return errorId
    }


}