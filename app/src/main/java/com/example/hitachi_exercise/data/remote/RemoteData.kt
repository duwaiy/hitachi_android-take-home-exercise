package com.example.hitachi_exercise.data.remote

import com.example.hitachi_exercise.data.Resources
import com.example.hitachi_exercise.data.dto.CityResponse
import com.example.hitachi_exercise.data.remote.service.ServiceApiServer
import com.example.hitachi_exercise.data.remote.service.ServiceGeneratorAPIServer
import com.example.hitachi_exercise.utils.NetworkConnectivity
import retrofit2.Response
import java.io.IOException
import javax.inject.Inject

class RemoteData @Inject
constructor(
    private val serviceGenerator: ServiceGenerator,
    private val serviceGeneratorAPI: ServiceGeneratorAPIServer,
    private val networkConnectivity: NetworkConnectivity

) : RemoteDataSource {

    private suspend fun processCall(responseCall: suspend () -> Response<*>): Any? {
        if (!networkConnectivity.isConnected()) {
            return null
        }
        return try {
            val response = responseCall.invoke()
            val responseCode = response.code()
            if (response.isSuccessful) {
                response.body()
            } else {
                response
            }
        } catch (e: IOException) {
            null
        }
    }

    override suspend fun getCity(key: String?,
                                 q: String?,
    ): Resources<CityResponse> {
        val serviceApi = serviceGenerator.createService(ServiceApiServer::class.java)

        return when (val response = processCall {
            serviceApi.getCity(
                key,
                q
            )
        }) {
            is CityResponse -> {
                Resources.Success(data = response)
            }

            else -> {
                if (response != null) {
                    var ress = response as Response<*>
                    Resources.DataError(
                        errorCode = ress.code(),
                        errorDesc = ress.errorBody()!!.string()
                    )
                } else {
                    Resources.DataError(
                        errorCode = 408,
                        errorDesc = "Network error, could get data, please try again!"
                    )
                }
            }
        }
    }


}