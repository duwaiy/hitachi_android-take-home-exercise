package com.example.hitachi_exercise.data.remote.service

import androidx.annotation.Keep
import com.example.hitachi_exercise.data.dto.CityResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

@Keep
interface ServiceApiServer {
    @GET("v1/current.json")
    suspend fun getCity(
        @Query("key") key: String?,
        @Query("q") q: String?,
    ): Response<CityResponse>
}