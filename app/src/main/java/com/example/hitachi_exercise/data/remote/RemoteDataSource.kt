package com.example.hitachi_exercise.data.remote

import com.example.hitachi_exercise.data.Resources
import com.example.hitachi_exercise.data.dto.CityResponse

interface RemoteDataSource {
    suspend fun getCity(
        key: String?,
        q: String?,

    ): Resources<CityResponse>
}