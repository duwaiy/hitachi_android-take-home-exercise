package com.example.hitachi_exercise.ui.pages

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.ctcorp.trv.transvisionxgo.data.remote.moshi.MyKotlinJsonAdapterFactory
import com.example.hitachi_exercise.data.DataRepositorySource
import com.example.hitachi_exercise.data.Resources
import com.example.hitachi_exercise.data.dto.CityResponse
import com.example.hitachi_exercise.data.remote.moshi.MyStandardJsonAdapters
import com.example.hitachi_exercise.ui.base.BaseViewModel
import com.example.hitachi_exercise.utils.wrapEspressoIdlingResource
import com.squareup.moshi.Moshi
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@HiltViewModel
class MainViewModel @Inject
constructor(private val dataRepositoryRepository: DataRepositorySource) : BaseViewModel() {
    val moshi: Moshi = Moshi.Builder()
        .add(MyKotlinJsonAdapterFactory())
        .add(MyStandardJsonAdapters.FACTORY)
        .build()

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    val submitCityDataPrivate = MutableLiveData<Resources<CityResponse>>()
    val submitCityLiveData: LiveData<Resources<CityResponse>> get() = submitCityDataPrivate

    fun getCity(
        key: String?,
        q: String?
    ) {
        viewModelScope.launch {
//            submitCityDataPrivate.value = Resources.Loading()
//            wrapEspressoIdlingResource {
                dataRepositoryRepository.getCity(
                    key,q
                ).collect { value ->
                    submitCityDataPrivate.value = value
                }
//            }
        }
    }


}