package com.example.hitachi_exercise.ui.pages

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.hitachi_exercise.R
import com.example.hitachi_exercise.databinding.ActivityInputBinding
import dagger.hilt.android.AndroidEntryPoint
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import com.example.hitachi_exercise.data.Resources
import com.example.hitachi_exercise.data.dto.CityResponse
import com.example.hitachi_exercise.ui.base.BaseActivity
import com.example.hitachi_exercise.utils.observe
import com.squareup.moshi.JsonAdapter
import org.json.JSONException
import java.io.UnsupportedEncodingException

@AndroidEntryPoint
class MainActivity : BaseActivity(), AdapterView.OnItemSelectedListener {

    private lateinit var binding: ActivityInputBinding
    private val mainViewModel by viewModels<MainViewModel>()
    var list_of_city = arrayOf("Kuala Lumpur", "Singapore")
    private var cityName: String? = ""
    private var key: String? = ""


    override fun observeViewModel() {
        observe(mainViewModel.submitCityLiveData, ::handleDataCity)

    }

    @SuppressLint("ShowToast")
    private fun handleDataCity(status: Resources<CityResponse>) {
        when (status) {
            is Resources.Loading -> {
                showLoading(true)
            }
            is Resources.Success -> status.data?.let {
                try {
                    Log.e("celsius", it?.current?.tempC.toString())
                    Log.e("fahrenhait", it?.current?.tempF.toString())

                    this.startActivity(Intent(this, DetailActivity::class.java).apply {
                        putExtra("celsius", it?.current?.tempC.toString())
                        putExtra("fahrenhait", it?.current?.tempF.toString())
                    })
                    showLoading(false)
                } catch (e: UnsupportedEncodingException) {
                    //Error
                    showLoading(false)

                } catch (e: Exception) {
                    //Error
                    showLoading(false)

                }

            }
            is Resources.DataError -> {
                showLoading(false)

                if (status.errorCode != 408 && !status.errorDesc.isNullOrEmpty()) {


                } else {
//                    status.errorDesc?.let { snackBar(it) }
                }
            }
        }
    }

    fun city(key: String?, q: String?) {
//        if (!isGoogle) {
        val sharedPreferences: SharedPreferences =
            this.getSharedPreferences("pref", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("key", binding.etApiKey.text.toString())
        editor.putString("1", cityName)
        editor.apply()
//        }

        showLoading(true)
        mainViewModel.getCity(
            key,
            q
        )
    }

    override fun initViewBinding() {
        binding = ActivityInputBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.spCity!!.setOnItemSelectedListener(this)

        val city = ArrayAdapter(this, android.R.layout.simple_spinner_item, list_of_city)
        city.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spCity!!.setAdapter(city)
        setListener()

    }
    private fun setListener() {
        binding.btSubmit.setOnClickListener(View.OnClickListener {

            key = binding.etApiKey.text.toString()
            (Log.e("data key", key!!))
            city(key, cityName)
        })
    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {

        cityName = list_of_city[position]
        (Log.e("data city", cityName!!))
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("Not yet implemented")
    }
    private fun showLoading(show: Boolean) {
        binding.llLoadingContent.visibility = if (show) View.VISIBLE else View.GONE
    }


}