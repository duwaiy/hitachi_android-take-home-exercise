package com.example.hitachi_exercise.ui.pages

import android.util.Log
import androidx.activity.viewModels
import com.example.hitachi_exercise.databinding.ActivityDetailBinding
import com.example.hitachi_exercise.ui.base.BaseActivity
import com.example.hitachi_exercise.utils.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailActivity : BaseActivity() {

    private var celcius: String? = ""
    private var fahrenhait: String? = ""


    private lateinit var binding: ActivityDetailBinding
    private val mainViewModel by viewModels<MainViewModel>()


    override fun observeViewModel() {
//        observe(mainViewModel.submitCityLiveData, ::handleDataCity)

    }

    override fun initViewBinding() {
        binding = ActivityDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        celcius = intent.getStringExtra("celsius")
        fahrenhait = intent.getStringExtra("fahrenhait")
        Log.e("Detail celsius", celcius.toString())
        Log.e("Detail fahrenhait", fahrenhait.toString())
        binding.etCelcius.setText(celcius)
        binding.etFahrenheit.setText(fahrenhait)
    }

}