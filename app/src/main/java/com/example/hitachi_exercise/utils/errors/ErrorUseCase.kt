package com.example.hitachi_exercise.utils.errors

import com.example.hitachi_exercise.data.error.Error

interface ErrorUseCase {
    fun getError(errorCode: Int,errorDesc : String): Error
}