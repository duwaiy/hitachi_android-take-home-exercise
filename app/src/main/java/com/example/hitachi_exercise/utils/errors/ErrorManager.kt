package com.example.hitachi_exercise.utils.errors

import javax.inject.Inject
import com.example.hitachi_exercise.data.error.Error

class ErrorManager @Inject constructor() : ErrorUseCase {
    override fun getError(errorCode: Int, errorDesc: String): Error {
        return Error(code = errorCode, description = errorDesc)
    }

}