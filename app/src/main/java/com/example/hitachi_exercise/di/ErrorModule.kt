package com.example.hitachi_exercise.di

import com.example.hitachi_exercise.data.error.mapper.ErrorMapper
import com.example.hitachi_exercise.data.error.mapper.ErrorMapperSource
import com.example.hitachi_exercise.utils.errors.ErrorManager
import com.example.hitachi_exercise.utils.errors.ErrorUseCase
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class ErrorModule {
    @Binds
    @Singleton
    abstract fun provideErrorFactoryImpl(errorManager: ErrorManager): ErrorUseCase

    @Binds
    @Singleton
    abstract fun provideErrorMapper(errorMapper: ErrorMapper): ErrorMapperSource
}